#include <stdio.h>
#include <stdbool.h>

int main() {

    int x = 0;

    int max = 0;

    while (true) {

        int n = scanf("%i", &x);

        if (n > 0) { // Successfully read an int

            printf("You entered %i\n", x);

        } else { // cannot read an int

            // flush the rest of the line

            int c = getc(stdin);

            while (c != '\n' && c != EOF) {

                c = getc(stdin);

            }

            if (c == EOF) {

                break;

            }

        }

        if(max<x){

            max = x;

        }

    }
    return 0;

}