#include <stdio.h>
#include <stdlib.h>
#include "pt_utils.h"
#define MAX_LENGTH 20
int main() {
    int nbits = 10;

    int arrayA[MAX_LENGTH] = {1,2,3,4,6,7,9,11,12,14,15,16,17,19,33,34,43,45,55,66};
    printf("arrayA: ");
    print_int_array(arrayA, 10);

    int arrayB[MAX_LENGTH] = {-1,-2,-3,-4,-6,-7,-9,-11,-12,-14,-15,-16,-17,-19,-33,-34,-43,-45,-55,-66};
    printf("arrayB:");
    print_int_array(arrayB, 10);

    //Create random array
    int *a = gen_rand_int_array(10, 5);

    //Clone the array that is just created
    int *clone = clone_int_array(a, MAX_LENGTH);

    printf("Array: ");

    print_int_array(clone,MAX_LENGTH);

    //Sort the array just created
    quick_sort(clone,0,MAX_LENGTH);

    printf("Array after sort: ");

    print_int_array(clone,MAX_LENGTH);

    //Test shuffle array
    printf("\nTest shuffle arrayA: ");
    shuffle_int_array(arrayA,nbits);
    print_int_array(arrayA,nbits);

    //Test Linear Search
    printf("\nLINEAR SEARCH:\n");
    linear_search(arrayA,MAX_LENGTH,15);

    //Test Binary Search
    printf("\nBINARY SEARCH\n");
    binary_search(arrayB,MAX_LENGTH,-14);
    binary_search(clone,MAX_LENGTH,pt_rand(nbits));

    free(clone);
    free(a);

}