#include <stdio.h>
#include <stdbool.h>

int main() {
    int x = 0;
    int firstNum;
    int position = 0;
    int count =0;

    while(true) {

        int n = scanf("%i", &x);

        if (n > 0) {//Successfully read an int

            if (position == 0){

                firstNum = x;

            } else if (x == firstNum) {

                count++;

                printf("The number %i was found at %i\n", firstNum, position);

            }

            position++;

        } else { //cannot read an int

                //flush the rest of the line

                int c = getc(stdin);

                while (c != '\n' && c != EOF) {

                    c = getc(stdin);

                }

                if (c == EOF) {

                    break;

                }

            }
        }

    if(count == 0){

        printf("Cannot find match!\n");

    }

    if(position>0){

        printf("Number %i was found %i time(s)\n", firstNum, count);

    }else{

        printf("No number was found!\n");

    }
    return 0;
}