#ifndef PT_UTILS
#define PT_UTILS

/** Generate a random int between 0 and 2^(nbits-1)
  * @param nbits the number of bits to use
  */
int pt_rand(int nbits);

/** Generate an array containing random int.
  * The array will be allocated on the heap.
  * @param length the length of the array
  * @param nbits the number of bits to pass to pt_rand
  */
int * gen_rand_int_array(int length, int nbits);

/** Copy the source array into the destination.
  * Assumes there is enough space in the destination to hold everything.
  * @param src the source array
  * @param dst the destination array
  * @param length the number of elements to copy
  */
void copy_int_array(int src[], int dst[], int length);

/** Clone an array.
  * The cloned array will be allocated on the heap.
  * @param a the array to clone
  * @param length the length of the array
  */
int * clone_int_array(int a[], int length);

/** Print an array of int.
  * @param a the array to print
  * @param length the length of the array
  */
void print_int_array(int a[], int length);

/** Shuffle an array of int.
 * @param a the array to print
 * @param length the length of the array
 */
void shuffle_int_array(int a[], int length);
/** Linear Search
 * @param a the array to search
 * @param length to search within the array
 * @param target to find
 */
int linear_search(int a[], int length, int target);
/** Binary search
 * @param a the array to search
 * @param length to search within the array
 * @param target to find
 * @return
 */
int binary_search(int a[], int length, int target);

/** Sort an int array using the selection sort algorithm.
  * Sort function number: 1
  * @param a the int array to be sorted in place
  * @param length the length of array a
  */
void selection_sort(int a[], int length);

/**
 *
 * @param arr
 * @param first
 * @param last
 * @return
 */
int partition(int arr[], int first, int last);
/**
 *
 * @param arr
 * @param first
 * @param last
 */
void quick_sort(int arr[], int first, int last);

#endif
