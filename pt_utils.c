#include <stdio.h>
#include <stdlib.h>

#include "pt_utils.h"

void swap(int *num1, int *num2){
    int temp = *num1;
    *num1 = *num2;
    *num2 = temp;
}

void shuffle_int_array(int a[], int length){
    for (int i = length - 1; i >= 1; --i)
        swap(&a[i], &a[rand() % (i + 1)]);// random element
}

int linear_search(int a[], int length, int target) {

    int index = -1;

    for (int i = 0; i < length; i++) {
        //Data found => break
        if (a[i] == target) {
            index = i;
            printf("%i is found at location %i of the array\n",target,index);
            break;
        }
    }
    if(index == -1){
        printf("%i cannot be found in the given array\n",target);
    }
    return index;
}

int binary_search(int a[], int length, int target) {
    int L = 0;
    int R = length -1;
    int mid = -1;
    int index = -1;

    while(L <= R) {
        mid = (L+R)/2;
        //Data found

        if (a[mid] == target) {

            index = mid;

            printf("%i is found at location %i of the array\n",target,index);

            break;

        } else {

                //Larger

            if (a[mid] < target) {

                L = mid + 1;

            } else {

                //Smaller

                R = mid - 1;

            }
        }
    }
    if(L > R){
        printf("%i cannot be found in the given array\n",target);
    }
    return index;
}

void selection_sort(int a[], int length) {
    int min_pos;

    /* find the min value in a[i] to a[length-1], and swap it with a[i] */
    for (int i = 0; i < length; i++) {
        min_pos = i;
        /* find the minimum value in what is left of the array */
        for (int j = i + 1; j < length; j++) {
            if (a[j] < a[min_pos]) {
                min_pos = j;
            }
        }
        swap(&a[i], &a[min_pos]);
    }
}

int pt_rand(int nbits) {

    int mask;

    if (0 < nbits && nbits < sizeof(int)*8) {
        // the least significant nbits bits will be 1, others will be 0
        mask = ~(~((unsigned int) 0) << nbits);
    }
    else {
        // the mask will be all ones
        mask = ~((unsigned int) 0);
    }
    // get the next random number and keep only nbits bits
    return rand() & mask;
}

int * gen_rand_int_array(int length, int nbits) {

    int * a = malloc(sizeof(int)*length);
    for (int i = 0; i < length; i++) {
        a[i] = pt_rand(nbits);
    }
    return a;
}

void copy_int_array(int src[], int dst[], int length) {

    for (int i = 0; i < length; i++) {
        dst[i] = src[i];
    }
}

int * clone_int_array(int a[], int length) {

    int * clone = malloc(sizeof(int)*length);
    copy_int_array(a, clone, length);

    return clone;
}

void print_int_array(int a[], int length) {

    for (int i = 0; i < length; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

//Quick sort code obtain from http://stackoverflow.com/questions/27852641/quicksort-an-array-in-c
int partition(int arr[], int first, int last)
{
    int pivot = arr[last]; // changed the pivot
    int low = first;
    int i = first; // changed
    while(i <= last-1 ){// or you can do for(i=first;i<last;i++)
        if(arr[i] < pivot){
            swap(&arr[i], &arr[low]);
            low++;
        }
        i++;
    }
    swap(&arr[last], &arr[low]);
    // after finishing putting all the lower element than the pivot
    // It's time to put the pivot into its place and return its position
    return low;
}

void quick_sort(int arr[], int first, int last)
{
    int pivot_pos;
    if(first < last){
        pivot_pos = partition(arr, first, last);
        quick_sort(arr, first, pivot_pos-1);
        quick_sort(arr, pivot_pos+1, last);
    }
}
