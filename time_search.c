//
// Created by Admin on 4/6/2017.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "pt_utils.h"

int main(int argc, char * argv[]) {

    if(argc < 4){
        printf("Not enough arguments!\n");
    }

    //Declare length
    int length = 10;

    //Declare the number of bits which is 10
    int nbits = 10;

    //Get length from argv[1]
    if (argc >= 2) {

        length = atoi(argv[1]);

    }
    //Get the random seed from argv[2]

    if (argc >= 3) {

        if (argv[2][0] == 't') {
            time_t current_time = time(NULL);
            srand(current_time);
            printf("time: %s\n", ctime(&current_time));
        }
        else {
            srand(atoi(argv[3]));
        }
    }


    if (argc >= 4) {

        //Create random array
        int *a = gen_rand_int_array(length, nbits);

        //Clone the array that is just created
        int *clone = clone_int_array(a, length);

        //Sort the array
        quick_sort(clone,0,length);

        //printf("Length of array: %i\n",length);
        //printf("Array:");
        //print_int_array(clone,length);

        long int totalLinearStime = 0;

        long int totalBinaryStime = 0;

        //LINEAR TIMING

        for (int i = 1; i <= atoi(argv[3]); i++) {
            //Declare target with nbits = 10
            int target = pt_rand(10);

            printf("\nLINEAR SEARCH: %i\n", i);

            //Start Timing
            time_t start_t = time(NULL);

            clock_t start_c = clock();

            linear_search(clone, length, target);

            time_t end_t = time(NULL);

            clock_t end_c = clock();

            //Finished Timing

            long int microTime = end_c- start_c;
            totalLinearStime += microTime;

            if(i == atoi(argv[3])){
                printf("\nLinear search total time (micros): %ld\n",totalLinearStime);

                printf("\nAverage linear search time (micros): %ld\n",totalLinearStime/atoi(argv[3]));
            }
        }

        printf("------------------------------------------------");

        //BINARY TIMING
        for (int i = 1; i <= atoi(argv[3]); i++) {

            //Target with nbits = 10
            int target = pt_rand(10);

            printf("\nBINARY SEARCH %i\n", i);

            //Start Timing
            time_t start_t = time(NULL);

            clock_t start_c = clock();

            binary_search(clone, length, target);

            time_t end_t = time(NULL);

            clock_t end_c = clock();

            //Finished Timing

            totalBinaryStime += (end_c - start_c);

            long int microTime = end_c - start_c;

            if(i == atoi(argv[3])){
                printf("\nBinary search total time (micros): %ld\n",totalBinaryStime);

                printf("\nAverage binary search time (micros): %ld\n",totalBinaryStime/atoi(argv[3]));
            }

        }

        free(clone);

        free(a);
    }

}
